package queue;



public class ArrayQueue<E> implements Queue<E> {
	
	private final static int INITCAP = 4;
	private int size;
	private E[] elements;
	private int first;
	
	public ArrayQueue() {
		elements = (E[]) new Object[INITCAP];
		size = 0;
		first = 0;
	}
	
	public int size() {
		return size;
	}
	
	public boolean isEmpty() {
		return size==0;
	}
	
	public E first() {
		if(isEmpty())
			return null;
		return elements[first];
	}
	
	public E dequeue() {
		if(isEmpty())
			return null;
		E etd = elements[first];
		elements[first] = null;
		first = (first+1) % elements.length;
		size--;
		if(elements.length >= 2*INITCAP && size < elements.length/4)
			changeCapacity(elements.length/2);
		return etd;
	}
	
	public void enqueue(E e) {
		if(size == elements.length)
			changeCapacity(2*size);
		elements[(first+size)%elements.length] = e;
		size++;
	}
	
	public void changeCapacity(int newCap) {
		E[] temp = (E[]) new Object[newCap];
		for(int i=0; i<size; i++) {
			int scrIndex = (first+i) % elements.length;
			temp[i] = elements[scrIndex];
			elements[scrIndex] = null;
		}
		elements = temp;
		first = 0;
	}
	
	
	//JUST FOR TESTING
		@Override
		public void showReverse() { 
		    if (size == 0)
			   System.out.println("Queue is empty."); 
			else
			   recSR(first);
	    } 
	    private void recSR(int f) { 
			if (elements[f] != null) { 
			   recSR(f+1); 
			   System.out.println(elements[f]); 
		     } 
	    } 
}
