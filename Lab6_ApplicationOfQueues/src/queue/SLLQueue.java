package queue;


public class SLLQueue<E> implements Queue<E> {

	private int size;
	private Node<E> first;
	private Node<E> last;

	public SLLQueue() {
		size = 0;
		first = null;
		last = null;
	}

	public int size() {
		return size;
	}

	public boolean isEmpty() {
		return size==0;
	}

	public E first() {
		if (isEmpty())
			return null;
		return first.getElement();
	}

	public E dequeue() {
		if(isEmpty())
			return null;
		E etd = first.getElement();
		Node<E> temp = first;
		first = first.getNext();
		temp.clean();
		size--;
		return etd;
	}

	public void enqueue(E e) {
		Node<E> nta = new Node<E>(e, null);
		if(isEmpty()) {
			first = nta;
			last = nta;
		}
		else {
			last.setNext(nta);
			last = nta;
		}
		size++;
	}



	//JUST FOR TESTING
	@Override
	public void showReverse() { 
		if (size == 0)
			System.out.println("Queue is empty."); 
		else
			recSR(first);
	} 
	private void recSR(Node<E> f) { 
		if (f != null) { 
			recSR(f.getNext()); 
			System.out.println(f.getElement()); 
		} 
	} 


	public class Node<E> {
		
		private E element;
		private Node<E> next;

		public Node (E elem, Node<E> n) {
			element = elem;
			next = n;
		}

		public Node() {
			element = null;
			next = null;
		}

		//Getters
		public E getElement() {return element;}
		public Node<E> getNext() {return next;}

		//Setters
		public void setElement(E e) {element = e;}
		public void setNext(Node<E> n) {next = n;}

		public void clean() {
			element = null;
			next = null;
		}
	}

}
