package theSystem;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import queue.ArrayQueue;

public class JobSimulation {

	private ArrayQueue<Job> inputQueue;
	private ArrayQueue<Job> processingQueue;
	private Job[] terminatedJobs;

	public JobSimulation() {
		inputQueue = new ArrayQueue<Job>();
		processingQueue = new ArrayQueue<Job>();
		terminatedJobs = null;
	}

	public double getAve(String file) {

		try {
			readFile(file);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		processingSystem();

		double average = 0;
		for(int i=0; i<terminatedJobs.length; i++) {
			average += (terminatedJobs[i].totalTime());
		}
		average = average/terminatedJobs.length;
		return average;
	}

	private void readFile(String f) throws FileNotFoundException {

		File txtfile = new File(f);
		
		if (!(txtfile.exists()))
			throw new FileNotFoundException("invalid file: " + f);

		Scanner file = new Scanner(txtfile);

		while(file.hasNextLine()) {
			String temp = file.nextLine();
			Integer tempArrivalTime = Integer.valueOf(temp.substring(0, temp.indexOf(",")));
			Integer tempServiceTime = Integer.valueOf(temp.substring(temp.indexOf(",")+1));
			Job tempJob = new Job(tempArrivalTime, tempServiceTime);
			inputQueue.enqueue(tempJob);
		}		
		file.close();
	}

	private void processingSystem() {

		terminatedJobs = new Job[inputQueue.size()];

		int time = 0; //current time
		int index = 0; //current index in the terminatedJobs array

		while (!inputQueue.isEmpty() || !processingQueue.isEmpty()) {

			if (!processingQueue.isEmpty()) {

				processingQueue.first().decreaseServiceTime();

				if(processingQueue.first().getServiceTime()==0) {
					processingQueue.first().setDepartureTime(time);
					terminatedJobs[index] = processingQueue.dequeue();
					index++;
				}

				else {
					processingQueue.enqueue(processingQueue.dequeue());
				}
			}

			if (!inputQueue.isEmpty() && inputQueue.first().getArrivalTime()==time) {
				processingQueue.enqueue(inputQueue.dequeue());
			}

			time++;
		}
	}



	private class Job {
		private int arrivalTime;
		private int serviceTime;
		private int departureTime;

		public Job(Integer arrival, Integer service) {
			arrivalTime = arrival;
			serviceTime = service;
		}

		public int getArrivalTime() {
			return arrivalTime;
		}

		public int getServiceTime() {
			return serviceTime;
		}

		public void setDepartureTime(int departureTime) {
			this.departureTime = departureTime;
		}

		public void decreaseServiceTime() {
			serviceTime--;
		}

		public int totalTime() {
			return departureTime - arrivalTime;
		}

	}
}
