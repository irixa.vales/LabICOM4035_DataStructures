package Tester;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Scanner;

import Lists.CostumerList;
import Restaurant.Costumer;
import Restaurant.Restaurant;

public class Main {

	public static void main(String[] args) throws IOException {

		File f = new File("input.txt");
		
		Scanner files = new Scanner(f);
		
		while(files.hasNextLine()) {
			
			CostumerList<Costumer> costumers = new CostumerList<Costumer>();
			Restaurant restaurant = new Restaurant();
			
			String file = files.next();
			Scanner test = new Scanner(new File(file));
			
			while(test.hasNextLine()) {
				
				test.useDelimiter(",");
				
				int t1 = Integer.parseInt(test.next());
				int t2 = Integer.parseInt(test.next());
				int t3 = Integer.parseInt(test.next());
				double t4 = Double.parseDouble(test.next());
				int t5 = Integer.parseInt(test.next());
				
				
				Costumer tempCostumer = new Costumer(Integer.parseInt(test.next()), Integer.parseInt(test.next()), 
						Integer.parseInt(test.next()), Double.parseDouble(test.next().substring(1)), Integer.parseInt(test.next()));
				
				costumers.add(tempCostumer);
				
//				String line = test.next();
//				System.out.println(line);
//				String[] s = line.split(",");
//				
//				for(int i=0; i<s.length; i++) {
//					System.out.println(s[i]);
//				}
//				
//				
//				Costumer tempCostumer = new Costumer(Integer.parseInt(s[0]), Integer.parseInt(s[1]), 
//						Integer.parseInt(s[2]), Double.parseDouble(s[3].substring(1)), Integer.parseInt(s[4]));
			}

			restaurant.setList(costumers);
			File outputFile = new File(file.substring(0, file.indexOf(".")) + ".out");
			restaurant.printOutputTo(outputFile);
			test.close();
		}
		files.close();
		
//		costumers.add(new Costumer(1, 1, 10, 2.30, 15));
//		costumers.add(new Costumer(2, 2, 2, 5.00, 10));
//		costumers.add(new Costumer(2, 3, 5, 1.20, 7));
//		costumers.add(new Costumer(2, 4, 10, 11.00, 15));
//		costumers.add(new Costumer(4, 5, 10, 2.30, 20));
//		
//		restaurant.setList(costumers);
//		restaurant.printOutputTo();
		
	}
}
