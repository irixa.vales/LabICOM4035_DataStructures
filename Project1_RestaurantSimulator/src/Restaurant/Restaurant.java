package Restaurant;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import Iterators.CostumerIterator;
import Iterators.MATIterator;
import Iterators.MAXIterator;
import Iterators.PACIterator;
import Iterators.PATIterator;
import Lists.CostumerList;
import Lists.DNode;

public class Restaurant {

	CostumerList<Costumer> list;
	CostumerIterator<Costumer> iterator;

	Double P0, P1, P2, P3, P4;
	Integer C0, C1, C2, C3, C4;

	public Restaurant() {
		list = null;
		iterator = null;
	}

	public Restaurant(CostumerList<Costumer> list) {
		this.list = list;
		iterator = null;
	}

	public void setList(CostumerList<Costumer> list) {
		this.list = list;
	}

	public void printOutputTo(File f) throws IOException {
		
		PrintWriter file = new PrintWriter(f);
		
		getP0();
		getPatApproach();
		getMatApproach();
		getMaxApproach();
		getPacApproach();
		
		file.write("Maximum profit possible: $" + String.format("%.2f",P0) + "\nMaximum number of served costumers possible: " + C0
				+ "\nPat's approach profit: $" + String.format("%.2f", P1) + "\nPat's approach number of disappointed costumers: " + C1
				+ "\nMat's approach profit: $" + String.format("%.2f", P2) + "\nMat's approach number of disappointed costumers: " + C2
				+ "\nMax's approach profit: $" + String.format("%.2f", P3) + "\nMax's approach number of disappointed costumers: " + C3
				+ "\nPac's approach profit: $" + String.format("%.2f", P4) + "\nPac's approach number of disappointed costumers: " + C4);

		file.close();

	}

	private void getP0() {
		double P = 0.0;
		int C = 0;
		DNode<Costumer> current = list.first();
		for(int i=0; i<list.size(); i++) {
			P += current.getElement().getCost();
			C++;
			current = current.getNext();
		}
		P0 = P;
		C0 = C;
	}

	private void getPatApproach() {
		iterator = new PATIterator<Costumer>();
		Object[] values = iterate();
		P1 = (Double) values[0];
		C1 = (Integer) values[1];
	}

	private void getMatApproach() {
		iterator = new MATIterator<Costumer>();
		Object[] values = iterate();
		P2 = (Double) values[0];
		C2 = (Integer) values[1];
	}

	private void getMaxApproach() {
		iterator = new MAXIterator<Costumer>();
		Object[] values = iterate();
		P3 = (Double) values[0];
		C3 = (Integer) values[1];
	}

	private void getPacApproach() {
		iterator = new PACIterator<Costumer>();
		Object[] values = iterate();
		P4 = (Double) values[0];
		C4 = (Integer) values[1];
	}

	private Object[] iterate() {
		Object[] values = new Object[2];
		Double profit = 0.0;
		Integer angryCostumers = 0;
		int time = 1;
		DNode<Costumer> current = list.first(); //current costumer from input list
		int size = 0; //number of costumer added to iterator
		while (size<list.size() || iterator.hasNext()) {
			while (size<list.size() && current.getElement().getArrivalTime() <= time) {
				iterator.add(current.getElement());
				current = current.getNext();
				size++;
			}

			if (iterator.hasNext()) {
				while (iterator.hasNext() && time-iterator.first().getArrivalTime() > iterator.first().getPatience()) {
					iterator.next();
					angryCostumers++;
				}
				
				if(iterator.hasNext()) {	
					time += iterator.first().getTimeToPrep();
					profit += iterator.first().getCost();
					iterator.next();
				}
			}
			else {
				time++;
			}
		}
		values[0] = profit;
		values[1] = angryCostumers;
		return values;
	}
}
