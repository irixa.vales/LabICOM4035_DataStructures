package Restaurant;

public class Costumer {

	private int id;
	private int arrivalTime;
	private int timeToPrep;
	private double cost;
	private int patience;
	
	public Costumer (int arrival, int id, int prepTime, double cost, int patience) {
		this.arrivalTime = arrival;
		this.id = id;
		this.timeToPrep = prepTime;
		this.cost = cost;
		this.patience = patience;
	}
	
	//returns true if costumer has been waiting longer than their waiting time
	public boolean isAngry() {
		return (patience == 0);
	}
	
	//decreases patience by one time if their order has not yet been taken
	public void decreasePatience() {
		patience--;
	}

	public int getId() {
		return id;
	}

	public int getArrivalTime() {
		return arrivalTime;
	}

	public int getTimeToPrep() {
		return timeToPrep;
	}

	public double getCost() {
		return cost;
	}

	public int getPatience() {
		return patience;
	}

}
