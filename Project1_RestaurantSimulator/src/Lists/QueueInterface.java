package Lists;

public interface QueueInterface<E> {

	public int size();
	public boolean isEmpty();
	public DNode<E> first();
	public DNode<E> last();
	public void add(E e);
	public E remove();
	
}
