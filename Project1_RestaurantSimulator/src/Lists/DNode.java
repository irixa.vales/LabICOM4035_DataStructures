package Lists;

public class DNode<E> {

	private E element;
	private DNode<E> next, previous;
	
	public DNode () {
		element = null;
		next = previous = null;
	}
	
	public DNode (E element) {
		this.element = element;
		next = previous = null;
	}
	
	public DNode (E element, DNode<E> next, DNode<E> previous) {
		this.element = element;
		this.next = next;
		this.previous = previous;
	}

	public E getElement() {
		return element;
	}

	public DNode<E> getNext() {
		return next;
	}

	public DNode<E> getPrevious() {
		return previous;
	}

	public void setElement(E element) {
		this.element = element;
	}

	public void setNext(DNode<E> next) {
		this.next = next;
	}

	public void setPrevious(DNode<E> previous) {
		this.previous = previous;
	}
	
	public void clean() {
		element = null;
		next = null;
		previous = null;
	}
}
