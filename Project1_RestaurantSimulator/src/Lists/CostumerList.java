package Lists;

public class CostumerList<E> implements QueueInterface<E> {

	private DNode<E> first, last;
	private int size;

	public CostumerList () {
		first = last = null;
		size = 0;
	}

	public int size() {
		return size;
	}

	public boolean isEmpty() {
		return size==0;
	}

	public DNode<E> first() {
		return first;
	}

	public DNode<E> last() {
		return last;
	}

	public void add(E costumer) {
		DNode<E> temp = new DNode<E>(costumer);
		if (isEmpty()) {
			first = temp;
			last = temp;
		}
		else {
			temp.setPrevious(last);
			last.setNext(temp);
			last = temp;
		}
		size++;
	}

	public E remove() {
		//if(isEmpty())
		DNode<E> temp = first();
		E etr = temp.getElement();
		first = first.getNext();
		temp.clean();
		size--;
		return etr;
	}
	
	public E remove(DNode<E> ntr) {
		if(size==1) {
			first = last = null;
			ntr.clean();
			size--;
			return null;
		}
		if(first.equals(ntr)) {
			E etr = ntr.getElement();
			first = ntr.getNext();
			first.setPrevious(null);
			ntr.clean();
			size--;
			return etr;
		}
		if(last.equals(ntr)) {
			E etr = ntr.getElement();
			last = last.getPrevious();
			last.setNext(null);
			ntr.clean();
			size--;
			return etr;
		}
		E etr = ntr.getElement();
		ntr.getPrevious().setNext(ntr.getNext());
		ntr.getNext().setPrevious(ntr.getPrevious());
		ntr.clean();
		size--;
		return etr;
	}
}
