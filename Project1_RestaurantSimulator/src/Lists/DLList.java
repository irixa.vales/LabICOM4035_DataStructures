package Lists;

public interface DLList<E> {

	public void add(E e);
	public int size();
	public DNode<E> first();
	public DNode<E> last();
}
