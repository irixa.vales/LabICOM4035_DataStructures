package Lists;

public class Queue<E> {

	private E[] arr;
	private int cap; //array capacity
	private int first;
	private int size;
	
	public Queue() {
		cap = 10;
		arr = (E[]) new Object[cap];
		first = 0;
		size = 0;
	}
	
	public Queue(int size) {
		cap = size;
		arr = (E[]) new Object[cap];
		first = 0;
		size =0;
	}
	
	public E first() {
		return arr[first];
	}
	
	public void enqueue(E element) {
		if(size >= cap-1)
			changeCap(size*2);
		arr[size] = element;
		size++;
	}
	
	public E dequeue() {
		E temp = arr[first];
		arr[first] = null;
		first++;
		if(cap-size > cap/2)
			changeCap(cap/2);
		return temp;
	}
	
	public boolean isEmpty() {
		return (first == size);
	}
	
	private void changeCap(int newCap) {
		E[] newArr = (E[]) new Object[newCap];
		for(int i=0; i<arr.length; i++) {
			newArr[i] = arr[i];
		}
		arr = newArr;
	}
	
	
	
	
	
	private class SNode<E> {
		private E element;
		private SNode<E> next;
		
		public SNode(E element, SNode<E> next) {
			this.element = element;
			this.next = next;
		}
		
		public SNode() {
			element = null;
			next = null;
		}
		
		//Getters
		public E getElement() {return element;}
		public SNode<E> getNext() {return next;}
		
		//Setters
		public void setElement(E element) {this.element = element;}
		public void setNext(SNode<E> next) {this.next = next;}
	}
	
}
