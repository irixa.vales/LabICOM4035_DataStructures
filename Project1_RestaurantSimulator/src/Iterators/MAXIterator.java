package Iterators;

import Lists.CostumerList;
import Lists.DNode;
import Restaurant.Costumer;

public class MAXIterator<E> implements CostumerIterator<E> {

	private CostumerList<Costumer> list;
	private DNode<Costumer> current;
	private int size;

	public MAXIterator () {
		list = new CostumerList<Costumer>();
		current = null;
		size = 0;
	}

	public boolean hasNext() {
		return size>0;
	}

	public Costumer next() {
		if(!hasNext())
			return null;
		DNode<Costumer> ntr = current;
		Costumer etr = current.getElement();
		remove(ntr);
		if(hasNext())
			current = getMax();
		return etr;
	}

	public Costumer first() {
		if(!hasNext())
			return null;
		return current.getElement();
	}

	public void add(Costumer costumer) {
		list.add(costumer);
		size++;
		current = getMax();
	}

	private void remove(DNode<Costumer> ntr) {
		list.remove(ntr);
		size--;
	}

	private DNode<Costumer> getMax() {
		DNode<Costumer> max = list.first();
		DNode<Costumer> curr = list.first();
		double maxProfit = max.getElement().getCost();
		for (int i=1; i<list.size(); i++) {
			curr = curr.getNext();
			if (curr.getElement().getCost() > maxProfit) {
				max = curr;
				maxProfit = max.getElement().getCost();
			}
		}
		return max;
	}
}
