package Iterators;

import Lists.CostumerList;
import Lists.DNode;
import Restaurant.Costumer;

public class PACIterator<E> implements CostumerIterator<E> {

	private CostumerList<Costumer> list;
	private DNode<Costumer> current;
	private int size;
	
	public PACIterator() {
		list = new CostumerList<Costumer>();
		current = null;
		size = 0;
	}
	
	public boolean hasNext() {
		return size>0;
	}
	
	public Costumer next() {
		if(!hasNext())
			return null;
		DNode<Costumer> ntr = current;
		Costumer etr = current.getElement();
		remove(ntr);
		if(hasNext())
			current = getMin();
		return etr;
	}

	public Costumer first() {
		return current.getElement();
	}

	public void add(Costumer costumer) {
		list.add(costumer);	
		current = getMin();
		size++;
	}

	private void remove(DNode<Costumer> ntr) {
		list.remove(ntr);
		size--;
	}
	
	private DNode<Costumer> getMin() {
		DNode<Costumer> min = list.first();
		DNode<Costumer> curr = list.first();
		double minJob = min.getElement().getTimeToPrep();
		for (int i=1; i<list.size(); i++) {
			curr = curr.getNext();
			if (curr.getElement().getTimeToPrep() < minJob) {
				min = curr;
				minJob = min.getElement().getTimeToPrep();
			}
		}
		return min;
	}
}
