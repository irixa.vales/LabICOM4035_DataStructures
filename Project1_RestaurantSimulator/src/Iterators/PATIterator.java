package Iterators;

import Lists.CostumerList;
import Lists.DNode;
import Restaurant.Costumer;

public class PATIterator<E> implements CostumerIterator<E> {

	private CostumerList<Costumer> list;
	private DNode<Costumer> current;
	
	public PATIterator() {
		list = new CostumerList<Costumer>();
		current = null;
	}
	
	public boolean hasNext() {
		return !(current==null);
	}
	
	public Costumer first() {
		if (!hasNext())
			return null; //TODO
		return current.getElement();
	}
	
	public Costumer next() {
		if (!hasNext())
			return null;
		DNode<Costumer> ntr = current;
		Costumer etr = current.getElement();
		current = current.getNext();
		remove(ntr);
		return etr;
	}
	
	public void add(Costumer costumer) {	
		if(!hasNext()) {
			list.add(costumer);
			current = list.first();
		}
		else {
			list.add(costumer);
		}
	}
	
	private void remove(DNode<Costumer> ntr) {
		list.remove(ntr);
	}
}