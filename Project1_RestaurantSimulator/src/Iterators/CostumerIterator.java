package Iterators;

import Lists.DNode;
import Restaurant.Costumer;

public interface CostumerIterator<E> {
	
	public boolean hasNext();
	public Costumer next();
	public Costumer first();
	public void add(Costumer c);
	//public void remove(DNode<Costumer> n);
}
