package Iterators;

import Lists.CostumerList;
import Lists.DNode;
import Restaurant.Costumer;

public class MATIterator<E> implements CostumerIterator<E> {

	private CostumerList<Costumer> list;
	private DNode<Costumer> current;
	private int size;

	public MATIterator () {
		list = new CostumerList<Costumer>();
		current = null;
		size = 0;
	}

	public boolean hasNext() {
		return size>0;
	}

	public Costumer next() {
		if(!hasNext())
			return null;
		DNode<Costumer> ntr = current;
		Costumer etr = current.getElement();
		remove(ntr);
		if(hasNext()) {
			current = list.last();
		}
		return etr;
	}

	public Costumer first() {
		if(!hasNext())
			return null;
		return list.last().getElement();
	}
	
	public void add(Costumer costumer) {
		list.add(costumer);
		current = list.last();
		size++;
	}

	private void remove(DNode<Costumer> ntr) {
		list.remove(ntr);
		size--;
	}
}
