package classes;

public class Calculator {

	private String mathExp; //math expression
	private ArrayStack<String> stack;

	public Calculator (String expression) {
		mathExp = expression;
		stack = new ArrayStack<String>();
	}

	public double evaluate () throws IllegalArgumentException {

		int i=0;

		while (i<mathExp.length()) {

			char currChar = mathExp.charAt(i);

			if (Character.isNumber(currChar)) {
				String number = "";

				do {
					number += currChar;
					i++;
					if (i==mathExp.length())
						break;
					currChar = mathExp.charAt(i);
				} while (Character.isNumber(currChar));

				stack.push(number);
			}

			else if (Character.isOperator(currChar)) {

				if (Character.highPriority(currChar)) {
					String operator = "" + currChar;
					String nextNum = "";
					i++;
					currChar = mathExp.charAt(i);
					if (i<mathExp.length()) {
						do {
							nextNum += currChar;
							i++;
							if (i==mathExp.length())
								break;
							currChar = mathExp.charAt(i);
						} while (Character.isNumber(currChar));
					}

					stack.push(Character.eval(stack.pop(), operator, nextNum));
				}

				else {
					if(isFirstOperator()) {
						String operator = "" + currChar;
						stack.push(operator);
						i++;
					}

					else {
						String num2 = stack.pop();
						String oper = stack.pop();
						String num1 = stack.pop();
						stack.push(Character.eval(num1, oper, num2));
						String nextOperator = "" + currChar;
						stack.push(nextOperator);
						i++;
					}
				}
			}
			else
				throw new IllegalArgumentException("invalid operator: " + currChar);
		}
		String num2 = stack.pop();
		String oper = stack.pop();
		String num1 = stack.pop();
		return Double.valueOf(Character.eval(num1, oper, num2));
	}

	private boolean isFirstOperator() {
		return (stack.size()==1);
	}
}

