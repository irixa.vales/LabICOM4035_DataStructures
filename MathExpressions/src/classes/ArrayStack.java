package classes;

import interfaces.Stack;

public class ArrayStack<E> implements Stack<E> {

	private E[] stack;
	private int topPointer;
	
	public ArrayStack () {
		topPointer = -1; //top pointer is -1 when there are no elements in stack
		stack = (E[]) new Object[1];
	}
	
	public void push(E e) {
		topPointer++;
		if(isFull())
			doubleArrSize();
		stack[topPointer] = e;
	}
	
	public E pop() {
		if(isEmpty())
			return null;
		E etd = stack[topPointer];
		topPointer--;
		if(isHalfEmpty())
			decreaseArrSize();	
		return etd;
	}

	public int size() {
		return topPointer+1;
	}
	
	public E peek() {
		if(isEmpty())
			return null;
		return stack[topPointer];
	}
	
	public boolean isEmpty() {
		return (size()==0);
	}
	
	
	private boolean isFull() {
		return (size()==stack.length);
	}
	
	private boolean isHalfEmpty() {
		return (size()<(stack.length/2));
	}
	
	private void doubleArrSize() {
		E[] stack2 = (E[]) new Object[stack.length*2];
		for(int i=0; i<stack.length; i++) {
			stack2[i] = stack[i];
		}
		stack = stack2;
	}
	
	private void decreaseArrSize() {
		E[] stack2 = (E[]) new Object[stack.length];
		for(int i=0; i<(stack.length/2); i++) {
			stack2[i] = stack[i];
		}
		stack = stack2;
	}
}
