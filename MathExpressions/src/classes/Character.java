package classes;

public class Character {

	
	/**Determines if the character is a number
	 * @param c the character
	 * @return true if c is a number, false if not
	 */
	public static boolean isNumber (char c) {
		String num = "" + c;
		for (int i=0; i<10; i++) {
			if (num.equals(String.valueOf(i)))
				return true;
		}
		return false;
	}
	
	
	/**Determines if the character is a valid operator
	 * @param c the character
	 * @return true if it is a valid operator, false if not
	 */
	public static boolean isOperator (char c) {
		String oper = "" + c;
		return (oper.equals("+") || oper.equals("-") || oper.equals("*") || oper.equals("/"));
	}
	
	
	/**Determines if the operator is high priority in the order of operations
	 * @param s the character, which must be a valid operator
	 * @return true if the operation is multiplication or division, false if not
	 */
	public static boolean highPriority (char s) {
		String operator = "" + s;
		return (operator.equals("*") || operator.equals("/"));
	}
	
	
	/**Evaluates a simple operation
	 * @param n1 first number
	 * @param o operation to execute between n1 and n2
	 * @param n2 second number
	 * @return String of the double value of result
	 */
	public static String eval (String n1, String o, String n2) {
		double number1 = Double.valueOf(n1);
		double number2 = Double.valueOf(n2);
		if (o.equals("+"))
			return String.valueOf(number1+number2);
		
		else if (o.equals("-"))
			return String.valueOf(number1-number2);
		
		else if (o.equals("*"))
			return String.valueOf(number1*number2);
		
		else
			return String.valueOf(number1/number2);
	}
}
