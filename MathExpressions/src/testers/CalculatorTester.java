package testers;

import java.util.Scanner;

import classes.Calculator;

public class CalculatorTester {

	public static void main (String[] args) {
		
		//Uncomment to test
		/*
		String e1 = "2+2+2";
		String e2 = "6+8*5-6";
		String e3 = "9*7+6*2";
		
		Calculator expr1 = new Calculator(e1);
		Calculator expr2 = new Calculator(e2);
		Calculator expr3 = new Calculator(e3);
		
		System.out.println("Calculator Tester:\n");
		System.out.println(e1 + " = " + expr1.evaluate());
		System.out.println(e2 + " = " + expr2.evaluate());
		System.out.println(e3 + " = " + expr3.evaluate());
		*/
		

		System.out.println("Enter math expression to evaluate:");
		Scanner scan = new Scanner(System.in);
		String expr = scan.next();
		Calculator expr1 = new Calculator(expr);
		System.out.println(expr1.evaluate());

	}
}
