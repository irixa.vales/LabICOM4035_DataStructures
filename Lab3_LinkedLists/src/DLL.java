
public class DLL<E> {

	private Node<E> head;
	private Node<E> tail;
	private int size=0;
	
	//Getters
	public Node<E> getHead() { return head; }
	public Node<E> getTail() { return tail; }
	public int getSize() { return size; }
	
	//Setters
	public void setHead(Node<E> h) { head = h; }
	public void setTail(Node<E> t) { tail = t; }
	public void setSize(int s) { size = s; }
	
	public DLL () {
		head = new Node<E>(null, null, null);
		tail = new Node<E>(null, null, null);
		head.setNext(tail);
		tail.setPrevious(head);
	}
	
	//Adds new element to first position
	public void addFirst (E element) {
		Node<E> temp = new Node<E>(element, head.getNext(), head);
		head.setNext(temp);
		temp.getNext().setPrevious(temp);
		size++;
	}
	
	//Adds new element to last position
	public void addLast (E element) {
		Node<E> temp = new Node<E>(element, tail, tail.getPrevious());
		tail.setPrevious(temp);
		temp.getPrevious().setNext(temp);
		size++;
	}
	
	//Deletes the first element
	public void deleteFirst() throws IllegalArgumentException {
		if(size==0) {
			throw new IllegalArgumentException("cannot delete first element: list is empty");
		}
		head.setNext(head.getNext().getNext());
		head.getNext().setPrevious(head);
		size--;
	}
	
	//Deletes the last element
	public void deleteLast() {
		if(size==0) {
			throw new IllegalArgumentException("cannot delete last element: list is empty");
		}
		tail.setPrevious(tail.getPrevious().getPrevious());
		tail.getPrevious().setNext(tail);
		size--;
	}
	
	//Replaces the first element that has oldValue with newValue
	public void replaceFirstByValue (E oldValue, E newValue) throws IllegalArgumentException{
		Node<E> temp = head;
		do {
			temp = temp.getNext();			
		} while (!oldValue.equals(temp.getElement()));
		temp.setElement(newValue);
	}
	
	//Replaces the last element that has oldValue with newValue
	public void replaceLastByValue (E oldValue, E newValue) throws IllegalArgumentException {
		Node<E> temp = tail;
		do {
			temp = temp.getPrevious();
		} while(!temp.getElement().equals(oldValue));
		temp.setElement(newValue);	
	}
	
	//Replaces all the elements that have oldValue with newValue
	public void replaceAllByValue (E oldValue, E newValue) {
		Node<E> temp = head;
		for (int i=0; i<size; i++) {
			temp = temp.getNext();
			if(temp.getElement().equals(oldValue))
				temp.setElement(newValue);
		}
	}
	
	//replaces the element at index with newValue
	public void replaceAtIndex (int index, E newValue) throws IllegalArgumentException {
		if(index>size) {
			throw new IllegalArgumentException("invalid index: " + index);
		}
		Node<E> temp = head;
		for (int i=0; i<index; i++) {
			temp = temp.getNext();
		}
		temp.setElement(newValue);
	}
}
