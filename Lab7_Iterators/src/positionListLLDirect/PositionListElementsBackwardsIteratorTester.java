package positionListLLDirect;

public class PositionListElementsBackwardsIteratorTester {

	public static void main(String[] args) {
		
		NodePositionList<Integer> list = new NodePositionList<Integer>();
		for(int i=1; i<=10; i++) {
			list.addFirst(i);
		}
		PositionListElementsBackwardsIterator<Integer> iterator = new PositionListElementsBackwardsIterator<Integer>(list);
		
		while(iterator.hasNext()) {
			System.out.println(iterator.next());
		}
	}
}
