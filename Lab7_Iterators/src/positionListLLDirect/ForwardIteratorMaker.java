package positionListLLDirect;

import positionInterfaces.PositionList;
import java.util.Iterator;

public class ForwardIteratorMaker<E> implements PositionListIteratorMaker<E> {

	public Iterator<E> makeIterator (PositionList<E> pl) {
		PositionListElementsIterator<E> iterator = new PositionListElementsIterator<E>(pl);
		return iterator;
	}
}
