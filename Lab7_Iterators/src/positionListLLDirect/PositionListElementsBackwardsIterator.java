package positionListLLDirect;

import java.util.Iterator;
import java.util.NoSuchElementException;

import positionInterfaces.Position;
import positionInterfaces.PositionList;

public class PositionListElementsBackwardsIterator<E> implements Iterator<E> {
	
	Position<E> current;
	PositionList<E> list;
	
	public PositionListElementsBackwardsIterator (PositionList<E> list) {
		this.list = list;
		if(list.isEmpty())
			current = null;
		else
			current = list.last();
	}
	
	public boolean hasNext() {
		return current != null;
	}
	
	public E next() throws NoSuchElementException {
		if (!hasNext())
			throw new NoSuchElementException("Iterator has past the end.");
		E etr = current.element();
		try {
			current = list.prev(current);
		} catch (Exception e) {
			current = null;
		}
		return etr;
	}
	
	public void remove() {
		try {
			current = list.prev(current);
			list.remove(list.prev(current));
		} catch (Exception e) {
			list.remove(current);
			current = null;
		}
	}
}
