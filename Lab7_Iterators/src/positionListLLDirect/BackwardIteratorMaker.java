package positionListLLDirect;

import positionInterfaces.PositionList;
import java.util.Iterator;

public class BackwardIteratorMaker<E> implements PositionListIteratorMaker<E> {
	
	public Iterator<E> makeIterator (PositionList<E> pl) {
		PositionListElementsBackwardsIterator<E> iterator = new PositionListElementsBackwardsIterator<E>(pl);
		return iterator;
	}
}
