package linkedLists;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class DLDHDTListIterableL2F<E> extends DLDHDTList<E> 
implements Iterable<E> 
{
	public Iterator<E> iterator() {
		return new LLIteratorF2L<E>(this);
	}

	private static class LLIteratorF2L<E> 
	implements Iterator<E> 
	{ 
		private LinkedList<E> theList;   // the list to iterate over
		// ... other internal fields ...
		private Node<E> current; 
		private boolean hasMoreElements; 

		public LLIteratorF2L(LinkedList<E> list) {
			theList = list; 

			if (theList.length()==0) {
				hasMoreElements = false;
				current = null;
			}
			else {
				hasMoreElements = true;
				current = theList.getLastNode();
			}
		}

		public boolean hasNext() {
			return hasMoreElements; 
		}

		public E next() throws NoSuchElementException {
			if (!hasNext())
				throw new NoSuchElementException("list has no more elements");
			E etr = current.getElement();
			try {
				current = theList.getNodeBefore(current);
			} catch (NodeOutOfBoundsException e) {
				current = null;
				hasMoreElements = false;
			}
			return etr;
		}

		public void remove() {
			if (!hasNext()) 
				throw new NoSuchElementException("list has no more elements");
			theList.removeNode(current);
		}
	}

}
