package classes;

public class SymmetricStringAnalyzer {
	private String s; 
	public SymmetricStringAnalyzer(String s) {
		this.s = s; 
	}
	
	/**
	 * Determines if the string s is symmetric
	 * @return true if it is; false, otherwise. 
	 */
	public boolean isValidContent() { 
		LLStack<Character> stack = new LLStack<Character>();
		for(int i=0; i<s.length(); i++) {
			char c = s.charAt(i);
			if(Character.isLetter(c)) {
				
				if(Character.isUpperCase(c))
					stack.push(c);
				
				else if(stack.isEmpty())
					return false;
				
				else {
					if(stack.top().equals(Character.toUpperCase(c)))
						stack.pop();
					
					else
						return false;
				}
			}
			
			else
				return false;
		}
		
		return (stack.isEmpty());
	}

	
	public String toString() { 
		return s; 
	}
	

	public String parenthesizedExpression() 
	throws StringIsNotSymmetricException 
	{
		if(!isValidContent()) {
			throw new StringIsNotSymmetricException("parenthesizedExpression: string is not symmetric");
		}
		
		String pe = new String(); //parenthesized expression

		for(int i=0; i<s.length(); i++) {
			char c = s.charAt(i);

			if(Character.isUpperCase(c))
				pe = pe+" <"+c;

			else {
				pe = pe+" "+c+">";
			}
		}
		
		return pe;  // need to change if necessary....
	}

}
