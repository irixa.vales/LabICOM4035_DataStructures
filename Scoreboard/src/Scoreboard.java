
public class Scoreboard {

	private int maxPlayers; //maximum number of entries a board may have
	private int totalPlayers; //total number of players currently in the board
	Player[] scoreboard;

	public Scoreboard(int n) {
		this.maxPlayers = n;
		this.totalPlayers = 0;
		this.scoreboard = new Player[maxPlayers];
	}

	public void addPlayer(String name, float score) {
		int i = totalPlayers;
		if(i==0) {
			scoreboard[0] = new Player(name, score);
			totalPlayers++;
		}
		else {
			if (!isNewPlayer(name))
				throw new IllegalArgumentException("name already on scoreboard: " + name);
			while(i>0 && score > scoreboard[i-1].getScore()) {
				try {
					scoreboard[i] = scoreboard[i-1];
					i--;
				}
				catch (IndexOutOfBoundsException e) {
					i--;
				}
			}
			try {
				scoreboard[i] = new Player(name, score);
			}
			catch (IndexOutOfBoundsException e) {

			}
			if(totalPlayers < maxPlayers) totalPlayers++;
		}
	}
	

	public void deletePlayer(String name) throws IllegalArgumentException {
		int i = getIndexOf(name);
		if(i==-1) {
			throw new IllegalArgumentException("invalid name: " + name);
		}
		while(i<totalPlayers-1) {
			scoreboard[i] = scoreboard[i+1];
			i++;
		}
	}
	

	public String getPlayerRankedAt(int n) throws IndexOutOfBoundsException {
		if (n<0 || n>totalPlayers) 
			throw new IndexOutOfBoundsException("invalid rank: " + n);
		return scoreboard[n-1].getName();
	}
	

	public int getRankOfPlayer(String player) throws IllegalArgumentException {
		if (getIndexOf(player)==-1) {
			throw new IllegalArgumentException("invalid name: " + player);
		}
		return getIndexOf(player)+1;
	}

	
	public void printScoreBoard(int n) throws IndexOutOfBoundsException {
		if (n>totalPlayers) {
			throw new IndexOutOfBoundsException("Not enough players to show on scoreboard");
		}
		System.out.println("NAME\tSCORE");
		for(int i=0; i < n; i++) { //Only top n scores will be printed
			System.out.println(scoreboard[i].getName() + "\t" + scoreboard[i].getScore());
		}
	}
	

	public boolean isNewPlayer(String name) {
		return (getIndexOf(name)==-1);
	}
	

	private int getIndexOf(String name) {
		for(int i=0; i<totalPlayers; i++) {
			if(name.equals(scoreboard[i].getName()))
				return i;
		}
		return -1;
	}
	
}
