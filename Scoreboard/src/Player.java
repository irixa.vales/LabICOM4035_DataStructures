
public class Player {

	private String name;
	private float score;
	
	public Player(String name, float score) {
		this.name = name;
		this.score = score;
	}
	
	//Getters
	public String getName() { return name; }
	public float getScore() { return score; }
	
	//Setters
	public void setName(String name) { this.name = name; }
	public void setScore(float score) { this.score = score; }
	
}
