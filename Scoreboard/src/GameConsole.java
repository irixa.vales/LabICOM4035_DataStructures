import java.util.Scanner;

public class GameConsole {


	public static void main(String[] args) {

		int menuChoice;
		Scoreboard bonusPoints = new Scoreboard(5);

		//Input created for testing purposes
		bonusPoints.addPlayer("Irixa", 10);
		bonusPoints.addPlayer("Amir", 5);
		bonusPoints.addPlayer("Mofongo", 1);
		bonusPoints.addPlayer("Osiris", 2);

		System.out.println("Bonus Points for Icom4035 Scoreboard");

		do {
			System.out.println("\nMenu:\n1-Add new player\n2-Delete player\n3-Show a player's rank\n"
					+ "4-Show the player in a specific rank\n5-Show scoreboard\n0-Quit");
			Scanner scan = new Scanner(System.in);
			menuChoice = scan.nextInt();
			switch(menuChoice) {
			case 0:
				break;
			case 1:
				System.out.println("Enter new player's name and score: ");
				String player = scan.next();
				float score = scan.nextFloat();
				bonusPoints.addPlayer(player, score);
				if(!bonusPoints.isNewPlayer(player)) {
					System.out.println("Successfully added player " + player + " with score: " + score);
				}
				else {
					System.out.println("Not a high score. Cannot be added to scoreboard");
				}
				break;
				
			case 2:
				System.out.println("Enter player you wish to delete: ");
				player = scan.next();
				bonusPoints.deletePlayer(player);
				System.out.println("Successfully deleted player " + player);
				break;
				
			case 3:
				System.out.println("Enter player you wish to know rank: ");
				player = scan.next();
				System.out.println("The rank of player " + player + " is: " + bonusPoints.getRankOfPlayer(player));
				break;
				
			case 4:
				System.out.println("Enter rank you wish to know: ");
				int rank = scan.nextInt();
				System.out.println("The player ranked number " + rank + " is: " + bonusPoints.getPlayerRankedAt(rank));
				break;
				
			case 5:
				System.out.println("Enter the number of players you wish to see in scoreboard: ");
				int n = scan.nextInt();
				bonusPoints.printScoreBoard(n);
				break;
				
			default:
				System.out.println("Invalid menu choice");
			}
		} while(menuChoice!=0);
	}
}
