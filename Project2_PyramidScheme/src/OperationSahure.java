import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

import positionalStructures.Position;

/**
 * 
 * This class contains the operations to run the simulation. It uses an algorithm
 * where a traversal is done to a tree so that every position is target0 once, and
 * then goes through every possible path. It prints the maximum assets seized possible
 * as well as the lists of members that need to be arrested in order to get that max
 * value.
 * 
 * @author Irixa
 *
 */
public class OperationSahure {

	private LinkedTree<Member> network;
	private ArrayList<String> arrestedLists;
	private int maxAssets;
	private PrintWriter writer;
	private int simsRun;

	/**
	 * Constructor. Creates new object with null network.
	 */
	public OperationSahure() {
		this.network = null;
		this.arrestedLists = new ArrayList<String>();
		this.maxAssets = 0;
		this.simsRun=0;
	}
	

	/**
	 * Runs the simulation for the tree that has been appointed to the OperationSahure object.
	 * Also creates and prints the output file corresponding to the number of sims run by the object.
	 * @param maxArrests- Number of maximum arrests possible on current network.
	 * @throws FileNotFoundException
	 * @throws UnsupportedEncodingException
	 */
	public void arrestNetwork(int maxArrests) throws FileNotFoundException, UnsupportedEncodingException {
		
		//If network is null, don't do anything other than print an error message.
		if(network == null){
			System.out.println("Network is null. Please set a valid LinkedTree to run simulation.");
			return;
		}
		
		//Advance the counter for sims that have been run (for output naming reasons).
		this.simsRun++;
		//Set a new PrintWriter with proper name.
		 writer = new PrintWriter("output"+simsRun+".txt", "UTF-8");
		 
		 //Outputs appropriate file if tree is empty.
		 if(network.isEmpty()){
			 writer.println("\n\nMaximum seized assets: 0");
			 writer.close();
			 return;
		 }
		 
		 
		//Start simulation from root and with max number of arrests for tree.
		startFromTarget(network.root(), maxArrests); 

		//Print same thing to file.
		writer.println("Maximum seized assets: " + maxAssets);
		//Print all arrestedLists items
		for(int i = 0; i < arrestedLists.size(); i++) {
			//Print same thing to file.
			writer.println("List " + (i+1) + ": " + arrestedLists.get(i));
		}
		//Close PrintWriter for current file.
		writer.close();
	}

	/**
	 * Makes a pre-order traversal on tree, making sure every member of the network is target0 exactly once.
	 * That means every member will be arrested first from the pyramid only once.
	 * @param t - target position, member that will be arrested first
	 * @param maxArrests - number of maximum members that can be arrested due to time limit
	 */
	private void startFromTarget(Position<Member> target0, int maxArrests) {
		int currAssets = 0;
		String currList = "";
		//Arrest current target 0.
		arrest(target0, maxArrests, currList, currAssets);
		//If target0 has children, run arrest sim starting at children as well.
		if(network.numChildren(target0) > 0) {
			for(Position<Member> child : network.children(target0)) {
				startFromTarget(child, maxArrests);
			}
		}
	}

	/**
	 * Runs the arrest simulation for position target, with remaining arrests n. recursively continues the sim
	 * for positions that target can reveal.
	 * 
	 * @param target  - new member to be arrested
	 * @param n - number of possible arrests left, considering the maximum number of arrests
	 * @param currList - current list of arrested members in order
	 * @param currAssets - seized assets
	 */
	private void arrest(Position<Member> target, int n, String currList, int currAssets) {

		//If all possible arrests have been made, nothing can be done
		if(n==0)
			return;
		
		//If target member is not already on the list
		if(!target.getElement().isArrested()) {
			//Set arrested member to true
			target.getElement().setArrested(true);
			
			//Add the name of the target member to the list
			if(!currList.isEmpty())
				currList += ", ";
			currList += target.getElement().getName();
			
			//Add the asset of the member to the total assets being seized
			currAssets += target.getElement().siezeAssets();
			
			//If no other arrest is possible (max arrests have been made or no other
			//available member to reveal).
			if((network.numChildren(target)<=0 
					&& target.getElement().getSponsor().getElement().isArrested()
					&& target.getElement().getMentor().getElement().isArrested())
					|| n<=1){

				//If assets are equal to the maximum seized assets, add list to arrestedLists
				if(currAssets == this.maxAssets) {
					this.arrestedLists.add(currList);
				}

				//If assets are greater than maximum seized assets,
				//remove all current lists, add the new one, and change maxAssets value
				else if(currAssets > this.maxAssets) {
					this.arrestedLists.removeAll();
					this.arrestedLists.add(currList);
					this.maxAssets = currAssets;
				}
			}
			
			//If there are members left to reveal.
			else {
				if(target.getElement().hasSponsor()) {
					//Arrest sponsor
					arrest(target.getElement().getSponsor(), n-1, currList, currAssets);
					//If sponsor is not mentor.
					if(target.getElement().getMentor() != target.getElement().getSponsor()) {
						//Arrest mentor
						arrest(target.getElement().getMentor(), n-1, currList, currAssets);
					}
				}

				//Arrest children if any.
				if(network.numChildren(target) > 0) {
					for(Position<Member> child : network.children(target)) {
						arrest(child, n-1, currList, currAssets);
					}
				}
			}

			//Set arrested to false to continue with list simulation.
			target.getElement().setArrested(false);
		}
	}

	/**
	 * Sets new Network to be used on new simulation and resets private instance variables.
	 * @param newTree - new tree to be used in simulation.
	 */
	public void setNetwork(LinkedTree<Member> newTree){
		this.network = newTree;
		this.maxAssets=0;
		this.arrestedLists.removeAll();
	}
}
