import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;


/**
 * Contains the main method to run and the required variables and method calls to run the simulation
 * according to project specifications.
 * @author Brian Rodriguez Badillo
 *
 */
public class Main {
	public static void main(String[] args){
		Input input = new Input();
		String inFile = "input.txt";
		Network<Member> tree = null;
		OperationSahure net = new OperationSahure();

		//Reads the input file defined by the inFile string variable. 
		input.readTextFile(inFile);

		//Simulation loop.
		int i=0;
		
		//Save time when simulation begins for testing purposes
		/*long initialTime = System.currentTimeMillis();*/
		
		while(input.logFiles[i]!=null){
			
			//Sets the tree to the current network;
			tree = input.readLogFile(input.logFiles[i]);

			//Sets the tree to be used for simulation in the net object.
			net.setNetwork(tree.getNetwork());
			
			//Runs the simulation with the appropriate max arrests and returns an array of output strings.
			
			try {
				net.arrestNetwork(input.logMaxArrests[i]);
			} 
			
			//Handle Exceptions if any.
			catch (FileNotFoundException e) {
				System.out.println("File was not found.");
				e.printStackTrace();
			} catch (UnsupportedEncodingException e) {
				System.out.println("Unsupported Encoding: Please select a different encoding.");
				e.printStackTrace();
			}

			i++;
		}
		
		//Prints total running time of the sim for testing purposes
		/*long finalTime = System.currentTimeMillis();
		System.out.println("Total running time of simulation: " + (double) (finalTime-initialTime)/1000);*/
		
	}
}
