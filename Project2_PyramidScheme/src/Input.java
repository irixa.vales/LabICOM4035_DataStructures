import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * Reads input txt files and log files and builds the required trees.
 * @author brian
 *
 */
public class Input {
	public String[] logFiles = new String[100];
	public int[] logMaxArrests = new int[100];	
	
	private int counter;

	/**
	 * Constructor.
	 */
	public Input(){
		counter=10;
		return;
	}

	/**
	 * Opens the file with fileName inside the project folder and saves
	 * the log filenames in the logFiles array.
	 * @param fileName - name of input.txt to read inside project folder.
	 */
	public void readTextFile(String fileName){
		String line = null;

		try {
			FileReader fileReader = new FileReader(fileName);
			BufferedReader bufferedReader = new BufferedReader(fileReader);
			int i=-1;
			int index;
			while((line = bufferedReader.readLine()) != null) {
				i++;
				//defines where to split the int and the string.
				index= line.indexOf(' ');
				//saves the int to the array.
				logMaxArrests[i] = Integer.parseInt((line.substring(0, index)));
				//saves the file name to the other array.
				logFiles[i] = line.substring(index+1, line.length());

				//resizes arrays if they are getting full.
				if(logFiles.length-15 <i){
					String[] temp = new String[logFiles.length*2];
					int[] temp2 = new int[temp.length];
					int j=0;
					for(String s: logFiles){
						temp[j] = s;
						j++;
					}
					logFiles=temp;
					j=0;
					for(int c: logMaxArrests){
						temp2[j] = c;
						j++;
					}
					logMaxArrests = temp2;
				}

			}   

			bufferedReader.close();
		}
		catch(FileNotFoundException ex) {
			System.out.println(
					"Unable to open file '" + 
							fileName + "'");                
		}
		catch(IOException ex) {
			System.out.println(
					"Error reading file '" 
							+ fileName + "'");                  
		}
	}

	/**
	 *  Opens the file with fileName inside the project folder and 
	 * creates a Network<Member> tree with the suspects in the file.
	 * @param fileName - name of file to read.
	 * @return - Network<Member> tree of suspects.
	 */
	public Network<Member> readLogFile(String fileName){
		if(fileName==null)return null;
		Network<Member> tree = new Network<Member>();
		BufferedReader br = null;
		String line = "";
		String logSplitBy = "#";
		String[] suspect = new String[3];

		try {
			br = new BufferedReader(new FileReader(fileName));
			
			//System.out.println("Network members:");
			while ((line = br.readLine()) != null) {
			//for(int k=0; k<memberCount; k++) {
				// use pound/hashtag as separator
				suspect = line.split(logSplitBy);
				//If member has no sponsor (is boss/root).
				if(suspect.length==2) tree.addMember(new Member(suspect[0], Integer.parseInt(suspect[1]), null));
				// Else, add it as a child.
				else  tree.addMember(new Member(suspect[0], Integer.parseInt(suspect[1]), suspect[2]));
			}


		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return tree;
	}
}
