
/**
 * Contains an array of elements.
 *
 * @param <E>
 */
public class ArrayList<E> {
	
	private static final int INITCAP = 1; 
	private static final int CAPTOAR = 1; 
	private static final int MAXEMPTYPOS = 2; 
	private E[] element; 
	private int size; 

	/**
	 * Constructor. Creates new ArrayList object
	 */
	public ArrayList() { 
		element = (E[]) new Object[INITCAP]; 
		size = 0; 
	} 


	/**
	 * Adds new element to list
	 * @param index - where element is to be added
	 * @param e - element to add
	 * @throws IndexOutOfBoundsException
	 */
	public void add(int index, E e) throws IndexOutOfBoundsException {
		if (index<0 || index>size) throw new IndexOutOfBoundsException("invalid index: " + index);
		if(size+1 >= element.length)
			changeCapacity(CAPTOAR);
		moveDataOnePositionTR(index, size-1);
		element[index] = e;
		size++;
	}


	/**
	 * Adds new element to last position
	 * @param e - element to add
	 */
	public void add(E e) {
		if (size == element.length)
			changeCapacity(CAPTOAR);
		element[size] = e;
		size++;
	}


	/**
	 * Returns element
	 * @param index - of element to be returned
	 * @return element at index
	 * @throws IndexOutOfBoundsException
	 */
	public E get(int index) throws IndexOutOfBoundsException {
		if (index<0 || index>size) throw new IndexOutOfBoundsException("invalid index: " + index);
		return element[index]; 
	}


	/**
	 * Returns if list is empty or not
	 * @return true if list is empty, false otherwise
	 */
	public boolean isEmpty() {
		return size == 0;
	}


	/**
	 * Removes an element from the list
	 * @param index - index of element to remove
	 * @return element to remove
	 * @throws IndexOutOfBoundsException
	 */
	public E remove(int index) throws IndexOutOfBoundsException {
		if (index<0 || index>size) throw new IndexOutOfBoundsException("invalid index: " + index);
		E e = element[index];
		moveDataOnePositionTL(index+1, size-1);
		size--;
		return e;
	}


	/**
	 * Sets the element of the given index from the list to new element
	 * @param index - index of element to change
	 * @param e - element to change
	 * @return element in new position
	 * @throws IndexOutOfBoundsException
	 */
	public E set(int index, E e) throws IndexOutOfBoundsException {
		if (index<0 || index>size) throw new IndexOutOfBoundsException("invalid index: " + index);
		element[index] = e;
		return element[index];
	}


	/**
	 * Returns size of list
	 * @return size of list
	 */
	public int size() {
		return size;
	}	

	/**
	 * Returns capacity of array
	 * @return length of array
	 */
	public int capacity() {
		return element.length;
	}
	
	/**
	 * Removes all elements from list
	 */
	public void removeAll() {
		element = (E[]) new Object[INITCAP];
		size = 0;
	}

	/**
	 * Changes the length of the array
	 * @param change - new length of array
	 */
	private void changeCapacity(int change) { 
		int newCapacity = element.length + change; 
		E[] newElement = (E[]) new Object[newCapacity]; 
		for (int i=0; i<size; i++) { 
			newElement[i] = element[i]; 
			element[i] = null; 
		} 
		element = newElement; 
	}


	/**
	 * Moves elements one position to the right
	 * useful when adding a new element with the add with two parameters
	 * @param low - lowest index to move
	 * @param sup - highest index to move
	 */
	private void moveDataOnePositionTR(int low, int sup) { 
		// pre: 0 <= low <= sup < (element.length - 1)
		for (int pos = sup; pos >= low; pos--)
			element[pos+1] = element[pos]; 
	}

	/**
	 * Moves elements one position to the left
	 * useful when removing an element from the list
	 * @param low - lowest index to move
	 * @param sup - highest index to move
	 */
	private void moveDataOnePositionTL(int low, int sup) { 
		// pre: 0 < low <= sup <= (element.length - 1)
		for (int pos = low; pos <= sup; pos++)
			element[pos-1] = element[pos]; 
	}

}
