import positionalStructures.Position;


/**
 * Represents member objects to be held in criminal network structure.
 * Each Member should have a name and assets, and a sponsor, except the Boss Member.
 * @author Brian Rodriguez Badillo
 *
 */
public class Member {
	private int assets=0;
	private Position<Member> sponsor=null;
	private Position<Member> mentor=null;
	private String name = null;
	private String sponsorName = null;
	
	private boolean isArrested;
	
	/**
	 * Creates empty Member object.
	 */
	public Member(){
		return;
	}
	
	/**
	 * Creates a Member object that holds the member's name, assets, and the name of their sponsor.
	 * @param name -  name of the member
	 * @param assets -  number of assets: int
	 * @param sponsor -  name of the member's sponsor
	 */
	public Member( String name, int assets, String sponsor){
		this.assets=assets;
		this.name= name;
		sponsorName = sponsor;
		
		isArrested = false;
	}
	
	/**
	 * Checks if member's sponsorName is not null.
	 * @return - if member has sponsor: boolean
	 */
	public boolean hasSponsor(){
		return (sponsorName!=null);
	}
	
	/**
	 * 
	 * @return - member's name: String
	 */
	public String getName(){
		return name;
	}
	
	/**
	 * 
	 * @return - Member's sponsor's name: String
	 */
	public String getSponsorName(){
		return sponsorName;
	}
	
	/**
	 * Checks if the member has a mentor.
	 * @return - if member has mentor: boolean
	 */
	public boolean hasMentor(){
		return mentor!=null;
	}
	
	/**
	 * Returns the position of the member's mentor. If 
	 * member does not have mentor, returns null.
	 * @return - Position of member's mentor.
	 */
	public Position<Member> getMentor(){
		return mentor;
	}
	
	/**
	 * Sets the member's Mentor to the position passed to the method.
	 * @param mentorPosition
	 */
	public void setMentor(Position<Member> mentorPosition){
		mentor=mentorPosition;
	}
	
	/**
	 * Sets the member's Sponsor to the position passed to the method
	 * @param sponsorPosition
	 */
	public void setSponsor(Position<Member> sponsorPosition){
		sponsor = sponsorPosition;
	}
	
	/**
	 * Returns the position of member's sponsor. If
	 * he has no sponsor, return null.
	 * @return -  position of sponsor
	 */
	public Position<Member> getSponsor(){
		return sponsor;
	}
	
	/**
	 * 
	 * @return - assets: int
	 */
	public int siezeAssets(){
		return assets;
	}
	
	
	/**
	 * @return- if member is arrested: boolean.
	 */
	public boolean isArrested() {
		return isArrested;
	}
	
	/**
	 * Sets Member as arrested.
	 * @param b - true: arrested, false: not arrested.
	 */
	public void setArrested(boolean b) {
		this.isArrested = b;
	}

}
