import positionalStructures.Position;
import treeInterfaces.AbstractTree;
/**
 * Container class that holds the LinkedTree object referring to the desired criminal network.
 * It also contains a method to add members to the tree as specified by the project guidelines and
 * display methods for testing purposes.
 * @author Brian Rodriguez Badillo
 *
 * @param <E>
 */
public class Network<E>{
	
	private LinkedTree<Member> criminalNetwork = new LinkedTree<Member>();
	
	/**
	 * Creates empty Network
	 */
	public Network(){
		return;
	}
	
	/**
	 * 
	 * @return - LinkedTree<Member> of criminal Network
	 */
	public LinkedTree<Member> getNetwork(){
		return criminalNetwork;
	}
	
	/**
	 * Adds member to the criminalNetwork LinkedTree<Member>. If the tree is empty and the member
	 * to add has no sponsor, adds the member as a root. Else, it searches the existing tree for 
	 * the current member-to-add's sponsor and adds it to the tree underneath the sponsor. Additionally,
	 * searches the corresponding members to set the member-to-add's mentor.
	 * @param person - Member to add to the criminalNetwork
	 */
	public void addMember(Member person){
		//Assuming the first member added to list is the root/boss.
		if(criminalNetwork.isEmpty() && !person.hasSponsor()) criminalNetwork.addRoot(person);
		//If it isn't
		else{
			//search for the sponsor
			for(Position<Member> p : criminalNetwork.positions()){
				//If p is person's sponsor
				if(p.getElement().getName().equals(person.getSponsorName())){
					//if p has no children, set it as person's mentor.
					if(criminalNetwork.isExternal(p)){
						person.setMentor(p);
					}
					//If p has children:
					else{
						//Set p's youngest child (most recently added child) as person's mentor.
						for(Position<Member> c: criminalNetwork.children(p)){
							person.setMentor(c);
						}
					}
					//Set the correct p as person's sponsor.
					person.setSponsor(p);
					//add the person to the network under p.
					criminalNetwork.addChild(p, person);
					break;
				}
			}
		}
	}
	
	///////////////////////////////////////////////////////////////////////
	// The following are miscellaneous methods to display the content of //
	// the Network ....        adapted from lab code                     //
	///////////////////////////////////////////////////////////////////////
	public void displayNetwork() {                                       //
		final int MAXHEIGHT = 100;                                       //
		int[] control = new int[MAXHEIGHT];                              //
		control[0]=0;                                                    //
		if (!criminalNetwork.isEmpty())                                  //
			recDisplay(criminalNetwork.root(), control, 0);              //
		else                                                             //
			System.out.println("Tree is empty.");                        //
	}                                                                    //
                                                                         //
	// Auxiliary Method to support display                               //
	protected void recDisplay(Position<Member> position,                 //
			int[] control, int level)                                    //
	{                                                                    //
		printPrefix(level, control);                                     //
		System.out.println();                                            //
		printPrefix(level, control);   									 //
		String mentor;													 //
		if (position.getElement().getMentor()==null)					 //
			mentor="no mentor";											 //
		else {															 //
	   mentor = position.getElement().getMentor().getElement().getName();//
		}																 //
		System.out.println("__("+position.getElement().getName()+        //
				", " +position.getElement().siezeAssets()+ ", " +		 //
				mentor +")");         								     //
		control[level]--;                                                //
		int nc = criminalNetwork.numChildren(position);                  //
		control[level+1] = nc;                                           //
		for (Position<Member>  p : criminalNetwork.children(position)) { //
			recDisplay(p, control, level+1);                             //
		}                                                                //
	}                                                                    //
                                                                         //
	// Auxiliary method to support display                               //
	protected static void printPrefix(int level, int[] control) {        //
		for (int i=0; i<=level; i++)                                     //
			if (control[i] <= 0)                                         //
				System.out.print("    ");                                //
			else                                                         //
				System.out.print("   |");                                //
	}                                                                    //
    ///////////////////////////////////////////////////////////////////////
	
}
