package orderedStructures;

public class Geometric extends Progression {

	private double commonFactor; 
	
	public Geometric(double firstValue, double commonFactor) { 
		super(firstValue); 
		this.commonFactor = commonFactor; 
	}
	
	@Override
	public double nextValue() {
		current = current * commonFactor; 
		return current;
	}
	
	public String toString() {
		return ("Geometric(" + super.firstValue() + ", " + this.commonFactor + ")");
	}
	
	@Override
	public double getTerm(int n) {
		return (super.firstValue() * Math.pow(this.commonFactor, n-1));
	}

}
