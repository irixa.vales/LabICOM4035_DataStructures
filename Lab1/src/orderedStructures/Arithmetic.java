package orderedStructures;

public class Arithmetic extends Progression {
	private double commonDifference; 
	
	public Arithmetic(double firstValue, double commonDifference) { 
		super(firstValue); 
		this.commonDifference = commonDifference; 
	}
	
	@Override
	public double nextValue() throws IllegalStateException {
		if(!firstValue) {
			throw new IllegalStateException("Invalid operation: cannot execute nextValue() before executing firstValue() once");
		}
		current = current + commonDifference; 
		return current;
	}
	
	public String toString() {
		return ("Arithmetic(" + super.firstValue() + ", " + this.commonDifference + ")");
	}
	
	@Override
	public double getTerm(int n) {
		return (super.firstValue() + this.commonDifference*(n-1));
	}

}
